import React, { useState, useEffect, useContext } from "react";
import NavBar from "../components/NavBar";
import "../css/register.css";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal2 from "sweetalert2";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [isDisabled, setIsDisabled] = useState(true);
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  useEffect(() => {
    if (email !== "" && password1 !== "" && password2 !== "" && password1 === password2) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password1, password2]);

  useEffect(() => {
    if (password1 !== password2) {
      setIsPasswordMatch(false);
    } else {
      setIsPasswordMatch(true);
    }
  }, [password1, password2]);

  function registerUser(event) {
    //prevent page reloading
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password1,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === false) {
          Swal2.fire({
            title: "Register unsuccessful!",
            icon: "error",
            text: "Email already exist",
          });
        } else {
          Swal2.fire({
            title: "Register successful!",
            icon: "success",
            text: "User successfully registered",
          });

          navigate("/login");
        }
      });
  }

  return (
    <div>
      <div className=" bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen">
        <NavBar />
        <section className="pt-[100px]">
          <form
            className="flex flex-col items-center max-w-[1096px] min-h-[790px]  mx-auto bg-[#FFFAD7] justify-center"
            onSubmit={(event) => registerUser(event)}
          >
            <h1 className="text-[56px] text-[#FF2171] font-bold" id="register-header">
              Register
            </h1>
            <section className="flex flex-col">
              <label
                htmlFor=""
                className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
              >
                Email:
              </label>
              <input
                type="text"
                className="rounded-full bg-[#FFE4A7] pl-[130px] pr-5 w-[670px] h-[79px] text-[30px] text-[#FF2171]"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              ></input>
            </section>
            <section className="flex flex-col">
              <label
                htmlFor=""
                className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
              >
                Password:
              </label>
              <input
                type="password"
                className="rounded-full bg-[#FFE4A7]  w-[670px] h-[79px] pl-[180px] pr-5 placeholder:text-[30px] placeholder:text-[#FF2171] text-[30px]"
                placeholder="(8+ characters)"
                value={password1}
                onChange={(e) => {
                  setPassword1(e.target.value);
                }}
              />
            </section>
            <section className="flex flex-col">
              <label
                htmlFor=""
                className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
              >
                Confirm Password:
              </label>
              <input
                type="password"
                className="rounded-full bg-[#FFE4A7]  w-[670px] h-[79px] pl-[282px] pr-5 placeholder:text-[30px] placeholder:text-[#FF2171] text-[30px]"
                value={password2}
                onChange={(e) => {
                  setPassword2(e.target.value);
                }}
              />
            </section>
            <h2 className="text-red-500" hidden={isPasswordMatch}>
              The password doesnt match
            </h2>

            <button
              type="Submit"
              className="rounded-full w-[244px] h-[79px] px-5 bg-[#FF2171] mt-[50px] text-[#FFE4A7] text-[30px]"
              disabled={isDisabled}
            >
              Register
            </button>
          </form>
        </section>
      </div>
    </div>
  );
};

export default Register;
