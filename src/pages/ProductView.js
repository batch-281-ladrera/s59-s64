import React from "react";
import NavBar from "../components/NavBar";
import { useParams, useNavigate } from "react-router-dom";

import { useEffect, useState } from "react";
import Swal2 from "sweetalert2";

const ProductView = () => {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const { id } = useParams();
  const [quantity, setQuantity] = useState(1);
  const [isDisable, setIsDisable] = useState(true);

  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setPrice(data.price);
        setDescription(data.description);
        setImageUrl(data.imageUrl);
      });
  }, []);

  useEffect(() => {
    if (quantity === 1) {
      setIsDisable(true);
    } else {
      setIsDisable(false);
    }
  }, [quantity]);

  function substractQuantity() {
    setQuantity(quantity - 1);
  }

  function addQuantity() {
    setQuantity(quantity + 1);
  }

  const createOrder = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/new`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: `${productId}`,
        quantity: quantity,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(data);
        if (data === true) {
          Swal2.fire({
            title: "Checkout successful!",
            icon: "success",
            text: "You're parcel will be delivered in the next 3-5 working days.",
          });
          navigate("/allproducts");
        } else {
          Swal2.fire({
            title: "Checkout unsuccessful",
            icon: "error",
            text: "Please try again!",
          });
        }
      });
  };

  return (
    <div className="pt-[100px] bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen">
      <NavBar />
      <div className="grid grid-cols-2 gap-[20px] bg-[#FFFAD7] h-[790px] w-[1320px] mx-auto rounded-lg">
        <div className="bg-[#FFE4A7] h-[578px] w-[578px] p-[12px] rounded-2xl justify-self-center place-self-center flex justify-center items-center">
          <div className="bg-white h-[523px] w-[523px]">
            <img src={imageUrl} alt="" className="w-[523px]" />
          </div>
        </div>
        <div className="place-self-start pt-[130px] pl-[30px] justify-self-start flex flex-col gap-5">
          <h1 className=" text-[#FF2171] text-[50px] font-bold">{name}</h1>
          <h1 className="text-[50px] font-bold">Php {price}.00</h1>
          <h2 className="italic text-[30px] pr-[20px]">{description}</h2>
          <div className="flex gap-5">
            <button
              className=" bg-[#FF2171] text-[#FFE4A7] w-[327px] h-[51px] text-[30px] rounded-full"
              onClick={() => createOrder(id)}
            >
              Checkout
            </button>
            <div className="bg-[#FF2171] w-[163px] h-[51px] flex justify-center rounded-full">
              <button
                className="flex-auto text-[30px] text-[#FFE4A7]"
                disabled={isDisable}
                onClick={substractQuantity}
              >
                -
              </button>
              <h1 className="flex-auto flex justify-center items-center text-[30px] text-[#FFE4A7]">
                {quantity}
              </h1>
              <button className="flex-auto text-[30px] text-[#FFE4A7]" onClick={addQuantity}>
                +
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductView;
