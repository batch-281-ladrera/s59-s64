import React, { useState, useEffect } from "react";
import NavBar from "../components/NavBar";
import ProductCard from "../components/ProductCard";

const AllProducts = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);
  return (
    <div className="pt-[100px] bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen">
      <NavBar />
      <div className="grid grid-cols-4 gap-[20px] bg-[#FFFAD7] h-[790px] w-[1320px] mx-auto rounded-lg">
        {products}
      </div>
    </div>
  );
};

export default AllProducts;
