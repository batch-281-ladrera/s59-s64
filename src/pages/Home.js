import React from "react";
import NavBar from "../components/NavBar";

import "../css/home.css";
import { Link } from "react-router-dom";
import DogHome from "../assets/dog-homepage.png";
import CatHome from "../assets/cat-homepage.png";

const Home = () => {
  return (
    <div>
      <NavBar />
      <div className="bg-gradient-to-b from-[#FFFAD7] to-[#FF90BB] min-h-screen flex flex-col gap-[30px] px-[12px] justify-center items-center xl:flex-row xl:gap-[70px]">
        <section className="flex flex-col gap-[10px] items-center md:items-start">
          <p id="paragraph-one-home">"Bringing joy to every wag and purr!"</p>
          <p id="paragraph-two-home">
            We have everything you require for your furry companions, including pet food and a wide
            selection of accessories for pets.
          </p>
          <Link
            className="bg-[#FF2171] w-[300px] h-[63px] text-[#FFFAD7] flex justify-center items-center"
            id="shop-button"
            to={"/allproducts"}
          >
            Shop Now
          </Link>
        </section>

        <section className="flex flex-col gap-[10px] xl:flex-row xl:gap-[40px]">
          <img src={CatHome} alt="Cat" id="cat-home" />
          <img src={DogHome} alt="Dog" id="dog-home" />
        </section>
      </div>
    </div>
  );
};

export default Home;
