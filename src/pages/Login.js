import React from "react";
import NavBar from "../components/NavBar";
import "../css/login.css";
import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import Swal2 from "sweetalert2";
import { useNavigate } from "react-router-dom";
import UserContext from "../UserContext";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);
  const { user, setUser } = useContext(UserContext);

  const navigate = useNavigate();

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log(data);
        // localStorage.getItem('token', data);

        //if statement to check whether the login is successful.
        if (data === false) {
          /*alert('Login unsuccessful!');*/
          //in adding sweetlaert2 you have to use the fire method
          Swal2.fire({
            title: "Login unuccessful!",
            icon: "error",
            text: "Check your login credentials and try again",
          });
        } else {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          /*alert('Login successful!');*/

          Swal2.fire({
            title: "Login successful!",
            icon: "success",
            text: "Welcome to Paws & Claws",
          });
          navigate("/");
        }
      });
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [email, password]);

  return (
    <div className=" bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen">
      <NavBar />
      <section className="pt-[100px]">
        <form
          className="flex flex-col items-center max-w-[1096px] min-h-[790px]  mx-auto bg-[#FFFAD7] justify-center"
          onSubmit={(e) => authenticate(e)}
        >
          <h1 className="text-[56px] text-[#FF2171] font-bold" id="login-header">
            Login
          </h1>
          <section className="flex flex-col">
            <label
              htmlFor=""
              className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
            >
              Email:
            </label>
            <input
              type="text"
              className="rounded-full bg-[#FFE4A7] pl-[130px] pr-5 w-[670px] h-[79px] text-[30px] text-[#FF2171]"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            ></input>
          </section>
          <section className="flex flex-col">
            <label
              htmlFor=""
              className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
            >
              Password:
            </label>
            <input
              type="password"
              className="rounded-full bg-[#FFE4A7]  w-[670px] h-[79px] pl-[180px] pr-5 placeholder:text-[30px] placeholder:text-[#FF2171] text-[30px]"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </section>
          <h2 className="mt-5">
            Not yet a user?{" "}
            <Link className="hover:text-[#FF2171]" to={"/register"}>
              Register Now
            </Link>
          </h2>
          <button
            type="Submit"
            className="rounded-full w-[244px] h-[79px] px-5 bg-[#FF2171] mt-[50px] text-[#FFE4A7] text-[30px]"
            disabled={isActive}
          >
            Login
          </button>
        </form>
      </section>
    </div>
  );
};

export default Login;
