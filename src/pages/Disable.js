import React, { useEffect } from "react";
import { useParams, Navigate } from "react-router-dom";
import Swal2 from "sweetalert2";

const Disable = () => {
  const { id } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          if (data === true) {
            Swal2.fire({
              title: "Update Successful",
              icon: "success",
            });
          } else {
            Swal2.fire({
              title: "Update Failed",
              icon: "error",
              text: "Update failed please try again!",
            });
          }
        }
      });
  }, []);

  return <Navigate to="/dashboard" />;
};

export default Disable;
