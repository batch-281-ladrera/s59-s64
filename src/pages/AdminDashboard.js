import React, { useState, useEffect } from "react";
import ProductRow from "../components/ProductRow";
import { Link } from "react-router-dom";
import NavBar from "../components/NavBar";

const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            console.log(product);
            return <ProductRow key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return (
    <div className="bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen">
      <NavBar />
      <h1 className="text-[#FFE4A7] text-[50px] text-center pt-[50px] font-bold">
        Admin Dashboard
      </h1>
      <section className="flex gap-[20px] justify-center">
        <Link
          className="w-[203px] h-[28px] bg-[#FF2171] text-[#FFE4A7] rounded-full flex justify-center items-center"
          to={"/newproduct"}
        >
          Add New Product
        </Link>
        <button className="w-[203px] h-[28px] bg-[#FFE4A7] text-[#FF2171] rounded-full">
          Show User's Orders
        </button>
      </section>
      <div className="flex justify-center mt-[32px]">
        <table className="w-[1320px] bg-[#FFFAD7]">
          <tr>
            <th className="w-[223px] text-[#FF2171] text-[30px]">Name</th>
            <th className="w-[650px] text-[#FF2171] text-[30px]">Description</th>
            <th className="w-[132px] text-[#FF2171] text-[30px]">Price</th>
            <th className="w-[158px] text-[#FF2171] text-[30px]">Availability</th>
            <th className="w-[158px] text-[#FF2171] text-[30px]">Actions</th>
          </tr>
          {products}
        </table>
      </div>
    </div>
  );
};

export default AdminDashboard;
