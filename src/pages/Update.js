import React, { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Swal2 from "sweetalert2";

const Update = () => {
  const { id } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const navigate = useNavigate();

  function update(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          Swal2.fire({
            title: "Update Successful",
            icon: "success",
          });
          navigate("/dashboard");
        } else {
          Swal2.fire({
            title: "Update Failed",
            icon: "error",
            text: "Update failed please try again!",
          });
        }
      });
  }
  return (
    <div className="bg-gradient-to-r from-[#FFFAD7] to-[#FF2171] min-h-screen grid grid-cols-1">
      <form
        className="flex flex-col items-center w-[1096px] h-[790px]  mx-auto bg-[#FFFAD7] justify-center justify-self-center place-self-center rounded-xl"
        onSubmit={(e) => update(e, id)}
      >
        <h1 className="text-[56px] text-[#FF2171] font-bold">Update Product</h1>
        <section className="flex flex-col">
          <label
            htmlFor=""
            className="text-[30px] relative left-[40px] top-[62px] text-[#FF2171] pointer-events-none"
          >
            Name
          </label>
          <input
            type="text"
            className="rounded-t-full bg-[#FFE4A7] pl-[130px] pr-5 w-[670px] h-[79px] text-[30px] text-[#FF2171]"
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></input>
        </section>
        <section className="flex flex-col">
          <label
            htmlFor=""
            className="text-[30px] relative left-[36px] top-[11px] text-[#FF2171] pointer-events-none"
          >
            Description
          </label>

          <textarea
            name=""
            id=""
            cols="43"
            rows="4"
            className="bg-[#FFE4A7] mt-[-51px] text-[30px] pt-[60px] text-[#FF2171] pl-[40px]"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </section>
        <section className="flex flex-col">
          <label
            htmlFor=""
            className="text-[30px] relative left-[40px] top-[10px] text-[#FF2171] pointer-events-none"
          >
            Price
          </label>
          <input
            type="number"
            className="rounded-b-full bg-[#FFE4A7]  w-[670px] h-[79px] mt-[-45px] pl-[110px] pr-5 placeholder:text-[30px] placeholder:text-[#FF2171] text-[30px] text-[#FF2171]"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
          />
        </section>
        <button
          type="Submit"
          className="rounded-full w-[244px] h-[79px] px-5 bg-[#FF2171] mt-[50px] text-[#FFE4A7] text-[30px]"
        >
          Update
        </button>
      </form>
    </div>
  );
};

export default Update;
