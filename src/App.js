import React, { useState, useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import PageNotFound from "./pages/PageNotFound";
import Update from "./pages/Update";
import Disable from "./pages/Disable";
import NewProduct from "./pages/NewProduct";
import Activate from "./pages/Activate";

import ProductView from "./pages/ProductView";
import AllProducts from "./pages/AllProducts";
import AdminDashboard from "./pages/AdminDashboard";
import { UserProvider } from "./UserContext";

const App = () => {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  //Function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // useEffect(()=> {
  //   console.log(user)
  // }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="/allproducts" element={<AllProducts />} />
            <Route path="/products/:id" element={<ProductView />} />
            <Route path="/update/:id" element={<Update />} />
            <Route path="/activate/:id" element={<Activate />} />
            <Route path="/disable/:id" element={<Disable />} />
            <Route path="newproduct" element={<NewProduct />} />
            <Route path="/dashboard" element={<AdminDashboard />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </BrowserRouter>
      </div>
    </UserProvider>
  );
};

export default App;
