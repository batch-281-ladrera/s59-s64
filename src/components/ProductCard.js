import React from "react";
import { Link } from "react-router-dom";

const ProductCard = ({ productProp }) => {
  const { _id, name, description, price, imageUrl } = productProp;
  return (
    <div className="self-center place-self-center">
      <div className="bg-[#FFE4A7] h-[243px] w-[243px] p-[12px] rounded-2xl">
        <Link className="bg-white h-[220px] w-[220px]" to={`/products/${_id}`}>
          <img src={imageUrl} alt="" />
        </Link>
      </div>
      <h1 className="text-[#FF2171] font-bold text-[24px]">{name}</h1>
      <h1 className="text-[black] text-[16px]">Php {price}.00</h1>
    </div>
  );
};

export default ProductCard;
