import React, { useContext } from "react";
import "../css/NavBar.css";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

const NavBar = () => {
  const { user } = useContext(UserContext);
  return (
    <div className="fixed top-0 left-0 list-none flex items-center gap-5 bg-[#FFE4A7] w-screen px-[73px] h-[64px]">
      <Link
        to={"/"}
        className="text-[24px] text-[#FF2171] font-bold flex items-center"
        id="paws-n-claws-logo"
      >
        Paws & Claws
      </Link>
      <Link to={"/allproducts"} className="text-[24px] text-[#FF2171] font-bold ml-auto">
        View Store
      </Link>
      {user.isAdmin ? (
        <Link to={"/dashboard"} className="text-[24px] text-[#FF2171] font-bold">
          Admin Dashboard
        </Link>
      ) : (
        <></>
      )}

      {user.id === null || user.id === undefined ? (
        <>
          <Link to={"/login"} className="text-[24px] text-[#FF2171] font-bold">
            Login
          </Link>
        </>
      ) : (
        <Link to={"/logout"} className="text-[24px] text-[#FF2171] font-bold">
          Logout
        </Link>
      )}
    </div>
  );
};

export default NavBar;
