import React, { useState } from "react";
import { Link } from "react-router-dom";

const ProductRow = ({ productProp }, isHidden) => {
  const { _id, name, description, price, isActive } = productProp;

  return (
    <>
      <tr className="h-[91px]">
        <th className="text-[24px] font-bold">{name}</th>
        <th>{description}</th>
        <th>Php {price}.00</th>
        <th>{isActive ? "Available" : "Not Available"}</th>
        <th>
          <div className="flex flex-col gap-3">
            <Link
              className="bg-[#FF2171] text-[#FFE4A7] w-[114px] mx-auto rounded-full"
              to={`/update/${_id}`}
            >
              Update
            </Link>
            {isActive ? (
              <Link
                className="bg-[#FFE4A7] text-[#FF2171] w-[114px] mx-auto rounded-full"
                to={`/disable/${_id}`}
              >
                Disable
              </Link>
            ) : (
              <Link
                className="bg-[#FFE4A7] text-[#FF2171] w-[114px] mx-auto rounded-full"
                to={`/activate/${_id}`}
              >
                Enable
              </Link>
            )}
          </div>
        </th>
      </tr>
    </>
  );
};

export default ProductRow;
